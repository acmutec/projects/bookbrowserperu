# BookBrowserPeru

Buscador de libros en libererías limeñas.

---

**TODO**:
- [ ] API
- [ ] Spiders
    - [x] Communitas
    - [x] Crisol
    - [x] Librería SUR
    - [ ] Íbero Librerías (página se encuentra en reparación)
    - [x] SBS
    - [ ] El Virrey

---

**Collaborators**:
- María Lovatón (@mlovatonv)
- Juan Luis Quispe (@juanquispe)
- César Salcedo (@csalcedo001)
- Diego Cánez (@dgcnz)