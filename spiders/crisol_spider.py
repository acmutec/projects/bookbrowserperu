from bs4 import BeautifulSoup
import requests

json = {'crisol': []}
target = input()
target.replace(' ', '+')
search = 'https://www.crisol.com.pe/catalogsearch/result/?cat=&q=' + target
r = requests.get(search)
soup = BeautifulSoup(r.text, 'lxml')
for product in soup.find_all('div', class_='product-info'):
    attrs = {'name': product.find('div', class_='product-name').find('a').get('title'),
             'ref': product.find('div', class_='product-name').find('a').get('href'),
             'price': product.find('span', class_='price').string}
    json['crisol'].append(attrs)

print(json)