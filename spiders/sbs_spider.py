import requests
from bs4 import BeautifulSoup

# https://www.sbs.com.pe/catalogsearch/result/index/?cat=0&p=1&q=harry+potter

def sbs_spider(search, page=1, last_result={"link": ""}):
	url = "https://www.sbs.com.pe/catalogsearch/result/index/?cat=0&p={}&q=".format(page)
	url += search

	results = {"sbs": []}


	request = requests.get(url)
	if request.status_code==200:
		all_product_infos = BeautifulSoup(request.text, "html.parser").find_all('div', class_='product-info')

		# next page can only be detected if books in new page are in the previous one
		if last_result["link"] == all_product_infos[-1].find("div", class_="product-name").find("a")["href"]: 
			return []

		for product_info in all_product_infos:
			product_name = product_info.find("div", class_="product-name").find("a")
			prices = product_info.find_all("span", class_="price")
			# prices[0].text returns: '\r\n                    S/.\xa065,00                '
			# to get 'S/.65,00' you need to remove whitespaces and escape sequences
			normal_price = prices[0].text.strip().encode('ascii','ignore').decode("utf8")
			online_price = prices[1].text.strip().encode('ascii','ignore').decode("utf8") if len(prices)==2 else None
			# If item is out of stock, page shows a <p class="availability">
			stock = False if product_info.find("p", class_="availability") else True

			book = {
			"name": product_name["title"],
			"link": product_name["href"],
			"normal_price": normal_price,
			"online_price": online_price,
			"in_stock": stock
			}

			results["sbs"].append(book)

		# add contents of next page to results 
		next_result = sbs_spider(search, page+1, results["sbs"][-1])
		if next_result:
			results["sbs"].extend(next_result["sbs"])
		return results


	else:
		print("Server return response error code {}".format(request.status_code))

print(sbs_spider(input("Book: ")))


