import requests
from bs4 import BeautifulSoup


def searchCommunitas(book):
    # http://www.communitas.pe/es/buscar?search_query=los+pilares+de+la+tierra&submit_search=Buscar&orderby=quantity&orderway=desc

    query = book.lower().replace(" ", "+")
    baseURL = "http://www.communitas.pe/es/buscar?"
    queries = {
        "search_query": query,
        "submit_search": "Buscar",
        "orderby": "quantity",
        "orderway": "desc",
        "p": 1
    }
    queryString = '&'.join(str(key)+'='+str(val)
                           for key, val in queries.items())
    URL = baseURL + queryString
    r = requests.get(URL)
    print(r.status_code)

    soup = BeautifulSoup(r.text, 'html.parser')
    print(soup.find("div", {"id": "pagination"}))

    # TODO: iterate through all pages
    for book_div in soup.find_all("li", {"class": "ajax_block_product"}):
        block_product = book_div.find("a", {"class": "product_title_link"})
        if block_product["title"] == book.upper():
            bookURL = block_product["href"]
            bookRequest = requests.get(bookURL)
            soupBook = BeautifulSoup(bookRequest.text, 'html.parser')

            data = {"precio": soupBook.find("span", {"id": "our_price_display"}).string,
                    "stock": soupBook.find("span", {"class": "availability"}) == "EN STOCK",
                    }

            table = soupBook.find("p", {"id": "datos_libro"})
            data["Autor"] = table.find(
                "strong", string="Autor:").find_next("a", {"class": "product_entity"}).string

            data["Año"] = table.find(
                "strong", string="Año:").find_next("a", {"class": "product_entity"}).string

            data["Editorial"] = table.find(
                "strong", string="Editorial:").find_next("a", {"class": "product_entity"}).string

            print(data)


def main():
    book = input("Write desired book: ")
    searchCommunitas(book)


if __name__ == "__main__":
    main()
