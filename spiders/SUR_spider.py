import requests
from bs4 import BeautifulSoup

x = input("Inserte el query aqui: ")
x.replace(' ','+')
link = "http://www.libreriasur.com.pe/busqueda/listaLibros.php?tipoBus=full&palabrasBusqueda="+x+"&boton=Buscar"
page = requests.get(link)
soup = BeautifulSoup(page.text,'html.parser')
matches = soup.find_all('div',class_='minificha')
for div in matches:
    b_link = div.find('a')
    b_link = b_link['href']
    b_link.replace('[', '')
    b_link.replace(']', '')
    b_link = "http://www.libreriasur.com.pe/" + b_link
    book_link = requests.get(b_link)
    book_page = BeautifulSoup(book_link.text,'html.parser')
    try:
        print("Edicion:",book_page.find("dd", itemprop="datePublished").contents[0])
    except:
        print("No se encontro la edicion")
    print("Codigo ISBN:", book_page.find("dd", itemprop="isbn").contents[0])
    #print(book_page.find(class_='datePublished'))
    print(div.find(class_='title').contents[0].contents[0])
    print(div.find(class_='creator').contents[0].strip())
    print(div.find(class_='precio').contents[1].contents[0])